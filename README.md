# repayment
金融领域还款算法
1. 等本等息org.xzheng.repayment.service.impl.RePayAlgorithmAverageCapital
2. 按月付息到期还本org.xzheng.repayment.service.impl.RePayAlgorithmMonth
3. 一次性还本付息org.xzheng.repayment.service.impl.RePayAlgorithmOnce
4. 等额本息org.xzheng.repayment.service.impl.RePayAlgorithmPrincipal