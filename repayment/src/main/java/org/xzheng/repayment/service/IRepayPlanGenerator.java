package org.xzheng.repayment.service;

import java.util.Date;
import java.util.Map;

import org.xzheng.repayment.entity.ReplayPlan;

public interface IRepayPlanGenerator {
	/**
	 * 生成还款计划
	 * 
	 * @param principal
	 *            本金
	 * @param yearRate
	 *            年利率
	 * @param totalmonth
	 *            投资月数
	 * @param beginDate
	 *            开始计息日
	 * @return 还款计划清单。key：期数,value：每月计划
	 */
	public Map<Integer, ReplayPlan> getRepayPlan(double principal, double yearRate, int totalmonth, Date beginDate);
}
