package org.xzheng.repayment.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.xzheng.repayment.entity.ReplayPlan;
import org.xzheng.repayment.service.IRepayPlanGenerator;

/**
 * 一次性还本付息算法.
 * 
 * <pre>
 到期一次性还本付息总额=投资本金×[1+年利率÷365×投资天数]
 * </pre>
 */
public class RePayAlgorithmOnce implements IRepayPlanGenerator {

	@Override
	public Map<Integer, ReplayPlan> getRepayPlan(double invest, double yearRate, int days, Date begin) {
		Map<Integer, ReplayPlan> map = new HashMap<Integer, ReplayPlan>();
		BigDecimal interest = new BigDecimal(invest).multiply(new BigDecimal(yearRate)).multiply(new BigDecimal(days))
				.divide(new BigDecimal(365), 2, BigDecimal.ROUND_HALF_UP);
		BigDecimal principal = new BigDecimal(invest);
		DateTime dt = new DateTime(begin);
		DateTime after = dt.plusDays(days - 1);

		ReplayPlan plan = new ReplayPlan(1, yearRate, interest, principal, begin, after.toDate(), days);
		map.put(1, plan);
		return map;
	}

	public static void main(String[] args) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		DateTime start = new DateTime(sdf.parse("2017-07-02"));

		Map<Integer, ReplayPlan> detailPlan = new RePayAlgorithmOnce().getRepayPlan(3000, 0.1, 10, start.toDate());
		System.out.println("一次性还本付息");
		for (int i = 1; i <= detailPlan.size(); i++) {
			ReplayPlan plan = detailPlan.get(i);
			System.out.println(plan);
		}
	}
}
