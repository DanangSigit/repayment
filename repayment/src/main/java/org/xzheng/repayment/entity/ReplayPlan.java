package org.xzheng.repayment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.xzheng.repayment.util.DateUtil;

/**
 * 还款计划
 * 
 * @author xzheng
 *
 */
public class ReplayPlan implements Serializable {
	private static final long serialVersionUID = -1314071461972171625L;
	/**
	 * 当前期数
	 */
	private int period;
	/**
	 * 利息
	 */
	private BigDecimal interest;
	/**
	 * 本金
	 */
	private BigDecimal principal;
	/**
	 * 本息和
	 */
	private BigDecimal totalAmount;

	/**
	 * 年利率
	 */
	private double rate;

	/**
	 * 计息天数
	 */
	private int days;
	/**
	 * 计息开始日
	 */
	private Date startDate;
	/**
	 * 计息结束日
	 */
	private Date endDate;

	/**
	 * @param period
	 *            当前期数
	 * @param rate
	 *            年利率
	 * @param interest
	 *            利息
	 * @param principal
	 *            本金
	 * @param startDate
	 *            计息开始日
	 * @param endDate
	 *            计息结束日
	 * @param days
	 *            计息天数
	 */
	public ReplayPlan(int period, double rate, BigDecimal interest, BigDecimal principal, Date startDate, Date endDate,
			int days) {
		super();
		this.period = period;
		this.interest = interest;
		this.principal = principal.setScale(2, BigDecimal.ROUND_HALF_UP);

		this.totalAmount = interest.add(principal);
		this.totalAmount = totalAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
		this.startDate = startDate;
		this.endDate = endDate;
		this.days = days;
		this.rate = rate;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}

	public BigDecimal getPrincipal() {
		return principal;
	}

	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		String str = "期数:" + period + ",年化利率:" + rate + ",利息:" + interest + ",本金:" + principal + ",总额:" + totalAmount
				+ ",计息天数:" + days + ",计息开始日期:" + DateUtil.dateToString(startDate, "yyyy-MM-dd") + ",计息结束日:"
				+ DateUtil.dateToString(endDate, "yyyy-MM-dd");
		return str;
	}

}
