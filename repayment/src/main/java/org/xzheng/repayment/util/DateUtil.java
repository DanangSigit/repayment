package org.xzheng.repayment.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.FastDateFormat;

public class DateUtil {

	public static String dateToString(Date date, String format) {
		FastDateFormat fdf = FastDateFormat.getInstance(format);
		if (null == date)
			return null;
		return fdf.format(date);
	}

	/**
	 * 增加月份，解决大小月最后一天问题
	 * 
	 * @param date
	 * @param month
	 * @return
	 */
	public static Date add_month(Date date, int month) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.MONTH, month);
		c.add(Calendar.DAY_OF_MONTH, -1);
		return c.getTime();
	}

}
